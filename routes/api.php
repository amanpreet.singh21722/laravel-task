<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\UserController;
use App\Http\Controllers\ArticleController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/



Route::post('/user-register', [UserController::class, 'user_register']);
Route::post('/login', [UserController::class, 'login']);

Route::middleware('auth:sanctum')->group(function () {
    Route::post('/get-profile', [UserController::class, 'getProfile']);
    Route::put('/update-profile', [UserController::class, 'edit_profile']);
    Route::post('/create-article', [ArticleController::class, 'create']);
    Route::get('/read-article', [ArticleController::class, 'read']);
    Route::patch('/update-article/{article}', [ArticleController::class, 'update']);
    Route::delete('/delete-article/{article}', [ArticleController::class, 'delete']);
    Route::post('/create-comment/{article}', [ArticleController::class, 'comments']);
});