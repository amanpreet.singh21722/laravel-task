<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Article;
use App\Models\Comments;
use Validator;
use Auth;

class ArticleController extends Controller
{
    public function create(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'title' => 'required|string',
            'post' => 'required|string'
        ]);

        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()], 422);
        }

        if (Auth::user()->role != 0) {
            return response()->json(['error' => 'User Unauthorized'], 403);
        }
        $article = Article::create([
            'user_id' => Auth::user()->id,
            'title' => $request->title,
            'post' => $request->post
        ]);

        return response()->json(['article' => $article], 201);
    }

    public function read()
    {
        if (Auth::user()->role == '0') {
            $data = Article::with('comments')->where('user_id', Auth::user()->id)->get();
        } else {
            $data = Article::with('comments')->get();
        }

        return response()->json(['articles' => $data], 200);
    }

    public function update(Request $request, Article $article)
    {
        if (Auth::user()->role != '0' || $article->user_id != Auth::user()->id) {
            return response()->json(['error' => 'User Unauthorized'], 403);
        }

        $validator = Validator::make($request->all(), [
            'title' => 'required|string',
            'post' => 'required|string'
        ]);

        if ($validator->fails()) {
            return response()->json(['erros' => $validator->errors()], 422);
        }

        $article->update($request->only(['title', 'post']));
        return response()->json(['message' => 'Updated', 'article' => $article], 200);
    }

    public function delete(Request $request, Article $article)
    {
        if (Auth::user()->role != '0' || $article->user_id != Auth::user()->id) {
            return response()->json(['error' => 'User Unauthorized'], 403);
        }

        $article->delete();

        return response()->json(['message' => 'Article deleted successfully']);
    }

    public function comments(Request $request, Article $article)
    {
        if (Auth::user()->role != '1') {
            return response()->json(['error' => 'User Unauthorized'], 403);
        }

        $validator = Validator::make($request->all(), [
            'comment' => 'required|string'
        ]);

        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()], 422);
        }

        Comments::create([
            'user_id' => Auth::user()->id,
            'article_id' => $article->id,
            'comment' => $request->comment 
        ]);

        $articles = Article::with('comments')->where('id', $article->id)->get();
        return response()->json(['article' => $articles], 200);
    }
}